# Manage a TypeScript-JavaScript monorepo with Nx

This guide will setup a TypeScript-JavaScript monorepo with Nx in order to manage multiple packages with their dependencies.

A _root_ workspace will manage _app1_ and _app2_ packages. The two packages will have common and specific dependencies.

## Relative works

- [Manage a TypeScript-JavaScript monorepo with npm](https://gitlab.com/artios-org/artios-testbedding/manage-a-typescript-javascript-monorepo-with-npm)

  A key difference between Nx and npm is TODO.

- [Manage a TypeScript-JavaScript monorepo with pnpm](https://gitlab.com/artios-org/artios-testbedding/manage-a-typescript-javascript-monorepo-with-pnpm)

  A key difference between Nx and pnpm is TODO.

## Prerequisites

- [Node.js](https://nodejs.org/) must be installed - Check the [nvm](https://gitlab.com/artios-org/artios-testbedding/manage-node.js-and-npm-versions-with-nvm) guide to manage Node.js and npm versions;
- Access to the [Manage a TypeScript-JavaScript monorepo with npm](https://gitlab.com/artios-org/artios-testbedding/manage-a-typescript-javascript-monorepo-with-npm) codebase.

## Guide

_Highly inspired by the [Adding Nx to Lerna/Yarn/PNPM/NPM Workspace - nx.dev](https://nx.dev/recipes/adopting-nx/adding-to-monorepo) guide._

### Get the codebase

Clone, fork or copy the [Manage a TypeScript-JavaScript monorepo with npm](https://gitlab.com/artios-org/artios-testbedding/manage-a-typescript-javascript-monorepo-with-npm) codebase so that you have it locally.

### Install Nx

In order to install Nx in an existing project, execute the following command.

```sh
# Add Nx to the existing monorepo
npx add-nx-to-monorepo
```

This command will display an output similar to the following one. Ignore all of the questions so we can manually create the Nx configuration later.

```
✔ Which of the following scripts need to be run in deterministic/topoglogical order? · No items were selected

✔ Which of the following scripts are cacheable? (Produce the same output given the same input, e.g. build, test and lint usually are, serve and start are not) · No items were selected

✔ Enable distributed caching to make your CI faster · No
```

The [`nx.json`](./nx.json) is created and need to be updated with our configuration.

### Configure Nx

The original pipeline is described as below.

> From the `@monorepo-with-npm/root` workspace, run the pipeline to ensure all applications works as expected.
>
> ```sh
> # Validate the code format
> npm run review:code:format --workspaces --include-workspace-root
>
> # Fix the code format
> npm run review:code:format:fix --workspaces --include-workspace-root
>
> # Build all applications (if applicable)
> npm run build --workspaces --if-present
>
> # Run all applications
> npm run start --workspaces
> ```

The order of execution is `Validate the code format` -> `Build the applications` -> `Run the applications`.

TODO

### Run commands in the workspace

TODO

Now that the workspace has been setup, commands can be ran for some or all of the directories. These are examples.

```sh
# For Node.js project `@monorepo-with-pnpm/app1` run `start` script
pnpm --filter @monorepo-with-pnpm/app1 start

# For Node.js projects `@monorepo-with-pnpm/app1` and `@monorepo-with-pnpm/app2` run `start` script in parallel
pnpm --parallel --filter @monorepo-with-pnpm/app1 --filter @monorepo-with-pnpm/app2 start

# Run `start` script for all sub-Node.js projects from the current directory
pnpm --filter "*" start

# Run `start` script for all Node.js projects from the root directory
pnpm --workspace-root --filter "*" start

# Run `start` script for all Node.js projects from the root directory
pnpm --workspace-root --filter "*" --if-present start
```

## Additional resources

- _None at the moment._

## License

This work is licensed under the [Non-Profit Open Software License version 3.0](https://opensource.org/licenses/NPOSL-3.0).
